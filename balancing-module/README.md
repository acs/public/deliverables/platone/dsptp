**UC1/UC2 - rule_based_logic**

Service directory: Access the balancer module code for platform integration.

Testing directory: Testing environment for the balancer code including the testing data, forecaster module, plotting, etc.

Inside each directory, there is an additional Readme.md file for additional guidance.

Use cases: control logic and operation mode
1. UC1: 𝑟𝑢𝑙𝑒−𝑏𝑎𝑠𝑒𝑑 𝑐𝑜𝑛𝑡𝑟𝑜𝑙 & 𝑟𝑒𝑎𝑙−𝑡𝑖𝑚𝑒 (𝑚𝑒𝑎𝑠𝑢𝑟𝑒𝑚𝑒𝑛𝑡−𝑏𝑎𝑠𝑒𝑑) -- release 1&2
2. UC1: 𝑟𝑢𝑙𝑒−𝑏𝑎𝑠𝑒𝑑 𝑐𝑜𝑛𝑡𝑟𝑜𝑙 & 𝑜𝑓𝑓𝑙𝑖𝑛𝑒 (𝑓𝑜𝑟𝑒𝑐𝑎𝑠𝑡−𝑏𝑎𝑠𝑒𝑑) -- release3
3. UC1: optimization 𝑐𝑜𝑛𝑡𝑟𝑜𝑙 & 𝑜𝑓𝑓𝑙𝑖𝑛𝑒 (𝑓𝑜𝑟𝑒𝑐𝑎𝑠𝑡−𝑏𝑎𝑠𝑒𝑑) -- release4
4. UC2: 𝑟𝑢𝑙𝑒−𝑏𝑎𝑠𝑒𝑑 𝑐𝑜𝑛𝑡𝑟𝑜𝑙 & 𝑟𝑒𝑎𝑙−𝑡𝑖𝑚𝑒 (𝑚𝑒𝑎𝑠𝑢𝑟𝑒𝑚𝑒𝑛𝑡−𝑏𝑎𝑠𝑒𝑑)


