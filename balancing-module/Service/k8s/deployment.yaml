apiVersion: apps/v1
kind: Deployment
metadata:
  name: #APP_NAME#
spec:
  replicas: 1
  strategy:
    # Recreate strategy
    # type: Recreate # Development only. No additional parameters needed. Mind: the max unavailable percentage should be possible (25% from 1 in 0.75, which is not possible)

    # Rolling update strategy
    # - maxSurge define how many pod we can add at a time
    # - maxUnavailable define how many pod can be unavailable during the rolling update
    #
    # Setting maxUnavailable to 0 would make sure we have the appropriate capacity during the rolling update.
    # You can also use percentage based value instead of integer.
    rollingUpdate:
      # Example using percentage
      # maxSurge: 25%
      # maxUnavailable: 25%
      # Example using integer
      maxSurge: 1
      maxUnavailable: 0
  selector:
    matchLabels:
      app: #APP_NAME#
  template:
    metadata:
      labels:
        app: #APP_NAME#
    spec:
      # securityContext: runAsUser and fsGroup
      #   - Bitnami images
      #   - Set runAsUser and fsGroup to 1001 for all bitnami images (verify Dockerfile in the source github project when finding issues with permissions).
      #   - Use the corresponding xxx_1001 StorageClass when persistent storage is required.
      # - Other images
      #   - runAsUser and fsGroup can be deleted (incl securityContext if last parameter)
      #   - Use the corresponding non-xxx_1001 StorageClass when persistent storage is required.
      #   - When the application is not running as user 0 (root), make sure the application is running as bitnami (1001,1001 with corresponding StorageClass)
      securityContext:
        runAsUser: 1001
        fsGroup: 1001
      containers:
      - name: #APP_NAME#
        image: #IMAGE#
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop: ["ALL"]
          readOnlyRootFilesystem: true
      # Probes:
      # The following probes are tuned to general workload, however needs to be tailored to more specific workload.
      # StartupProbe is for reference. Only used when a database needs to be loaded before it becomes ready.
      # Read the following document carefully to avoid surprises: https://blog.colinbreck.com/kubernetes-liveness-and-readiness-probes-how-to-avoid-shooting-yourself-in-the-foot/
        # startupProbe:
        #   httpGet:
        #     # Give a proper end-point where the service can be check against. / is template only.
        #     path: #APP_ENDPOINT#
        #    port: 8080
        #    scheme: HTTP
        #   failureThreshold: 10
        #   periodSeconds: 10
        readinessProbe:
          httpGet:
            # Give a proper end-point where the service can be check against. / is template only.
            path: /balancing/health
            port: 8080
          initialDelaySeconds: 20
          periodSeconds: 10
          timeoutSeconds: 3
          successThreshold: 1
          failureThreshold: 5
        livenessProbe:
          httpGet:
            # Give a proper end-point where the service can be check against. / is template only.
            path: /balancing/health
            port: 8080
          initialDelaySeconds: 30
          periodSeconds: 10
          timeoutSeconds: 3
          successThreshold: 1
          failureThreshold: 2
        resources:
          requests:
            cpu: 200m
            memory: 250Mi
          limits:
            cpu: 1
            memory: 1000Mi
        env:
          - name: BALANCING_USERNAME
            valueFrom:
              secretKeyRef:
                name: balancing-auth
                key: BALANCING_USERNAME
          - name: BALANCING_PASSWORD
            valueFrom:
              secretKeyRef:
                name: balancing-auth
                key: BALANCING_PASSWORD
        ports:
          - containerPort: 8080
        volumeMounts:
          - name: tmp
            mountPath: /tmp
      volumes:
      - name: tmp
        emptyDir: {}
      imagePullSecrets:
      - name: aks-mgt-acr
---
apiVersion: v1
kind: Service
metadata:
  name: #APP_NAME#
  labels:
    app: #APP_NAME#
spec:
  ports:
  - name: http
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app: #APP_NAME#
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: azure/application-gateway
    appgw.ingress.kubernetes.io/appgw-ssl-certificate: eontools
    appgw.ingress.kubernetes.io/ssl-redirect: "true"
  name: #APP_NAME#
spec:
  rules:
  - host: 3fb294e8-8b16-4144-a079-8ef7110b0c7f.eon.tools
    http:
      paths:
      - backend:
          service:
            name: #APP_NAME#
            port:
              number: 8080
        path: /
        pathType: Prefix
