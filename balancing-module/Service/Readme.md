**Rule_based and Optimiser_logics**  

**balancer_input_offline.json**

*UC_name* 
- If UC_name=UC1_rule_based_offline, Rule based balancer with forecast values (only UC1 and the non-optimal schedule)
- If UC_name=optimiser, Optimisation-based balancer (UC1,3,4--final SoF and Bulk can be added with boolean entries)


*UC_start*
- starting time for the balancing mechanism (schedule/setpoint output is calculated from this timestamp onward)

*UC_end*
- ending time for the balancing mechanism (schedule/setpoint output is calculated till this timestamp)

*Bulk*
1. with_bulk - boolean value to activate the optimization in presence of bulk energy window 
2. Bulk_start - starting time of the bulk window
3. Bulk_end - ending time of the bulk window 
4. Bulk_energy_kWh - Amount of energy to be delivered/received (Kwh)
    - Negative bulk_energy shows the delivery of energy and therefore bulk export 
    - Positive bulk_energy shows the reception of energy and therefore bulk import
     
- Bulk_start and Bulk_end should always be in the acceptable range, i.e., between UC_start and UC_end
- Entries 2-4 are mandatory but irrelevant when with_bulk=false

*measurements*
1. Time - timestamp of forecast input to the balancer in datetime-timezone format 
2. Preq_kW - requested power exchange in kW (not needed!)
3. Cal_iONS_kW  - forecast values 
4. delta_T - frequency of occurrence of forecast values in hours ( for : one hour = 1 / half an hour = 0.5)


*flex_specs:*
1. with_fsof - boolean value to activate optimization including the constraint for the final state of flexibility
1. initial_SOF - initial state of aggregated flexibility (SOC) in percentage at UC_start
2. final_SOF - final state of aggregated flexibility (SOC) in percentage at UC_end 
3. Pf_dis_kW - max dischargable power in kW
4. Pf_ch_kW - max chargable power in kW
5. min_SOF - minimum state of flexibility in percentage
6. max_SOF - maximum state of flexibility in percentage
7. Agg_cap_flex - full capacity of flexibility assets (100% SOF) in kWh

- initial_SOF and final_SOF should always be in the acceptable range, i.e., between min_SOF and max_SOF
- final_SOF is mandatory but irrelevant when with_fsof=false

**Outputs objects (output.json) :**

Includes Ptcb_kW -  setpoint for the aggregated flexibility based on the forecast values (Ptei_forecast) 

*output*
- Timestamp - timestamp of ptcb_kW output  
- Values - Ptcb setpoint in kW


## Deployment
The necessary resource definitions to deploy to k8s and the image registry can be found in the [RWTH GitLab instance](https://git-ce.rwth-aachen.de/acs/private/research/platone/wp5/balancing-module/balancing-deployment/-/tree/v3-deployment) 