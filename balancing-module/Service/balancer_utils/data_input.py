from typing import List, Optional, Union
import pandas as pd
from pydantic import BaseModel as PydBaseModel, Field, ValidationError, validator
from datetime import datetime, timedelta
from enum import Enum


class BaseModel(PydBaseModel):
    class Config:
        allow_population_by_field_name = True
        json_encoders = {datetime: lambda dt: dt.strftime("%Y-%m-%dT%H:%M:%SZ")}
        use_enum_values = True


class StrEnum(str, Enum):
    pass


class BalancingMethod(StrEnum):
    RULE_BASED = "rule_based"
    OPTIMIZER = "optimizer"


class Bulk(BaseModel):
    with_bulk: bool = Field(True)
    bulk_start: datetime = Field(..., alias="Bulk_start")
    bulk_end: datetime = Field(..., alias="Bulk_end")
    bulk_energy_kwh: float = Field(..., alias="Bulk_energy_kWh")


class Measurement(BaseModel):
    time: datetime = Field(..., alias="Time")
    preq_kw: float = Field(..., alias="Preq_kW")
    cal_ions_kw: float = Field(..., alias="Cal_iONS_kW")


# TODO add constraints
# - initial_SOF and final_SOF should always be in the acceptable range, i.e., between min_SOF and max_SOF
# - final_SOF is mandatory but irrelevant when with_fsof=false
class FlexSpecs(BaseModel):
    id: Optional[str]
    with_fsof: bool = Field(
        ...,
        description="Activate optimization including the constraint for the final state of flexibility",
    )
    initial_sof: float = Field(
        ...,
        alias="initial_SOF",
        description="initial state of aggregated flexibility (SOC) in percentage at UC_start",
    )
    final_sof: float = Field(
        ...,
        alias="final_SOF",
        description="Final state of aggregated flexibility (SOC) in percentage at UC_en",
    )
    pf_dis_kw: float = Field(
        ..., alias="Pf_dis_kW", description="Max dischargable power in kW"
    )
    pf_ch_kw: float = Field(
        ..., alias="Pf_ch_kW", description="Max chargable power in kW"
    )
    min_sof: float = Field(
        ..., alias="min_SOF", description="Minimum state of flexibility in percentage"
    )
    max_sof: float = Field(
        ..., alias="max_SOF", description="Maximum state of flexibility in percentage"
    )
    agg_cap_flex: float = Field(
        ...,
        alias="Agg_cap_flex",
        description="Full capacity of flexibility assets (100% SOF) in kWh",
    )
    charge_efficiency: float = Field(default=1.0, alias="Charge Efficiency")
    discharge_efficiency: float = Field(default=1.0, alias="Discharge Efficiency")


class Solver(StrEnum):
    GUROBI = "gurobi"
    IPOPT = "ipopt"


class InputData(BaseModel):
    # solver: Solver = Solver.IPOPT
    application: str
    uc_name: BalancingMethod = Field(..., alias="UC_name")
    uc_start: datetime = Field(..., alias="UC_start")
    uc_end: datetime = Field(..., alias="UC_end")
    id: str
    bulk: Bulk = Field(..., alias="Bulk")
    measurements: List[Measurement]
    flex_specs: Union[FlexSpecs, List[FlexSpecs]]

    @validator("uc_name", pre=True)
    def uc_name_to_enum(cls, value: str) -> str:
        if value.lower() == "optimiser":
            return "optimizer"
        return value

    @validator("measurements")
    def measurements_start_before_timewindow(cls, meas, values):
        uc_start = values["uc_start"]
        if uc_start < meas[0].time:
            raise ValueError(
                f"Measurements have to start at or before uc_start. Measurements start at {meas[0].time} uc_start was {uc_start}"
            )
        return meas

    @validator("measurements")
    def measurements_end_after_timewindow(cls, meas, values):
        uc_end = values["uc_end"]
        if uc_end > meas[-1].time:
            raise ValueError(
                f"Measurements have to end at or after uc_end. Measurements end at {meas[0].time} uc_end was {uc_end}"
            )
        return meas


def minutes_horizon(starttime: datetime, endtime: datetime) -> float:
    time_delta = endtime - starttime
    total_seconds = time_delta.total_seconds()
    minutes = total_seconds / 60
    return minutes


def transform_flex_percent_to_abs(flex_specs: Union[FlexSpecs, List[FlexSpecs]]):
    if isinstance(flex_specs, list):
        for flex in flex_specs:
            flex.min_sof /= 100
            flex.max_sof /= 100
            flex.initial_sof /= 100
            flex.final_sof /= 100
            flex.agg_cap_flex *= 3600
    else:
        flex_specs.min_sof /= 100
        flex_specs.max_sof /= 100
        flex_specs.initial_sof /= 100
        flex_specs.final_sof /= 100
        flex_specs.agg_cap_flex *= 3600
    return flex_specs


def measurements_to_df(
    meas: List[Measurement], start: datetime = None, end: datetime = None
) -> pd.DataFrame:
    df_forecasts = pd.json_normalize([mes.dict(by_alias=False) for mes in meas])
    df_forecasts.set_index("time", inplace=True)
    df_forecasts.index.freq = pd.infer_freq(df_forecasts.index)
    df_forecasts = df_forecasts.loc[start:end]
    return df_forecasts


def flex_to_df(flex_specs: Union[FlexSpecs, List[FlexSpecs]]) -> pd.DataFrame:
    if isinstance(flex_specs, List):
        df_flex = pd.json_normalize([flex.dict(by_alias=False) for flex in flex_specs])

    else:
        df_flex = pd.json_normalize(flex_specs.dict(by_alias=False))

    # Set index to ids if all flex nodes have an id, otherwise leave rangeindex
    if ~df_flex.id.isna().any():
        df_flex.set_index("id", inplace=True)
    return df_flex
