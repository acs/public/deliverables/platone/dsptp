from importlib.metadata import version
from typing import Dict, List, Tuple
import pandas as pd
from datetime import datetime
from pydantic import BaseModel as PydBaseModel, Field
from pyomo.opt import SolverStatus, TerminationCondition


class BaseModel(PydBaseModel):
    class Config:
        allow_population_by_field_name = True
        json_encoders = {datetime: lambda dt: dt.strftime("%Y-%m-%dT%H:%M:%SZ")}
        use_enum_values = True


class TimeseriesData(BaseModel):
    time: datetime = Field(..., alias="Timestamp")
    # XXX this alias should really be Value but it is probably to late to change api
    values: Dict[str, float] = Field(..., alias="Values")


class BalancerOutput(BaseModel):
    id: str
    version: str
    units: Dict[str, str] = Field(default_factory=dict)
    output: List[TimeseriesData]


# XXX not sure why it is structured like this but this is needed to keep the response the same
class BalancerOutputWrapper(BaseModel):
    status: str = Field(default="success")
    details: str = Field(default="ok")
    balancer_output: BalancerOutput = Field(..., alias="Balancer_output")


def prep_optimizer_output(
    import_profile: pd.Series,
    flex_profiles: pd.DataFrame,
    sof_profiles: pd.DataFrame,
    forecasts: pd.Series,
    df_flex: pd.DataFrame,
):
    result_overview = pd.DataFrame(
        index=forecasts.index,
        columns=["ptei_kw", "expected_ptei_kw"],
    )
    result_overview["ptei_kw"] = forecasts["cal_ions_kw"]

    result_overview["expected_ptei_kw"] = import_profile
    for col in flex_profiles.columns:
        result_overview[f"ptcb_kw_{col}"] = flex_profiles[col]
        result_overview[f"sof_kwh_{col}"] = (
            sof_profiles[col] * df_flex.agg_cap_flex[col] / 3600
        )

    return result_overview


def values_mapper(col_name: str):
    if col_name.startswith("ptcb_kw_"):
        return col_name.removeprefix("ptcb_kw_")
    return col_name


def df_to_output(output: pd.DataFrame, id: str, status:Tuple[SolverStatus, TerminationCondition]) -> BalancerOutputWrapper:
    value_cols = output.columns[
        output.columns.map(lambda col: col == "time" or col.startswith("ptcb_kw"))
    ]
    # TODO decide whether "ptcb_kw" should be included in the name
    # output = output[value_cols].rename(columns=values_mapper).to_dict(orient="index")
    output = output[value_cols].to_dict(orient="index")
    output = [{"time": time, "values": d} for time, d in output.items()]
    out = BalancerOutput(
        id=id,
        version=version("platone-balancing-module"),
        units={"time": "ISO8601", "Ptcb": "kW"},
        output=output,
    )

    wrapped_output = BalancerOutputWrapper(status=status[0], details=status[1], balancer_output=out)
    return out, status[0], status[1]
