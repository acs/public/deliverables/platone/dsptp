from typing import Tuple
import pandas as pd
from pyomo.environ import SolverFactory
from pyomo.core import *
from balancer_utils import data_input, data_output
from balancer_utils.data_input import (
    InputData,
    FlexSpecs,
    Bulk,
    BalancingMethod as BM,
)
from pyomo.opt import SolverStatus, TerminationCondition
from balancer_utils import data_output


# Constraints
# Rule1: P_power demand has to be met
# Rule2: PV power limitation
# Rule3: State of flexibility consistency
# Rule4: Initial state of flexibility
# Rule5/8: FLEX charge/discharge limits
# Rule6/9: FLEX state of flexibility limits
# Rule7: Export/Import limit
# Rule10: Final state of flexibility
# Rule11: Bulk energy delivery


def con_rule1(model, t):
    return float(model.P_Ptei_Forecast[t]) == model.P_TEI_Output[t] + sum(
        model.P_FLEX_Output[n, t] for n in model.N
    )


def con_rule3(model, n, t):
    return (
        model.SoF_FLEX[n, t + model.dT]
        == model.SoF_FLEX[n, t]
        - model.P_FLEX_Output[n, t] * model.dT.seconds / model.FLEX_Capacity[n]
    )


def con_rule4(model, n):
    return model.SoF_FLEX[n, model.start_time] == model.FLEX_SoF_Value[n]


def con_rule5(model, n, t):
    return -float(model.FLEX_Max_Charge_Power[n]) <= model.P_FLEX_Output[n, t]


def con_rule6(model, n, t):
    return float(model.FLEX_Min_SoF[n]) <= model.SoF_FLEX[n, t]


def con_rule7(model, t):
    # FIXME I don't think this works how it is supposed to
    # I assume this should use export_limit unless it is not set
    # if model.export_limit == True:
    if model.export_limit is not None:
        return -model.export_limit <= model.P_TEI_Output[t]
    else:
        return -100000000000 <= model.P_TEI_Output[t]


def con_rule8(model, n, t):
    return model.P_FLEX_Output[n, t] <= model.FLEX_Max_Discharge_Power[n]


def con_rule9(model, n, t):
    return model.SoF_FLEX[n, t] <= model.FLEX_Max_SoF[n]


def con_rule10(model, n):
    # return model.SoF_FLEX[n, endtime] == model.FLEX_Final_SoF[n]
    if model.with_fsof[n]:
        return model.SoF_FLEX[n, model.end_time] == model.FLEX_Final_SoF[n]
    else:
        return Constraint.Feasible


# def con_rule11(model, n):
#     return (
#         sum(model.P_FLEX_Output[n, t] * model.dT.seconds for t in model.T_bulk)
#         == -model.Bulk_Energy[0]
#     )

def con_rule11(model):
    return (sum(sum(model.P_FLEX_Output[n, t] * model.dT.seconds for t in model.T_bulk) for n in model.N) == -model.Bulk_Energy[0])

# Objective: Minimize the power exchange with the grid
def obj_rule(model):
    return sum(model.P_TEI_Output[t] * model.P_TEI_Output[t] for t in model.T)


def balancer_control(data: InputData):
    # delta_T = data.measurements[0].delta_t
    flex_specs = data_input.transform_flex_percent_to_abs(data.flex_specs)
    df_forecasts = data_input.measurements_to_df(
        data.measurements, start=data.uc_start, end=data.uc_end
    )
    if data.uc_name == BM.RULE_BASED:
        if isinstance(flex_specs, list):
            if len(flex_specs) == 1:
                flex_specs = flex_specs[0]
            else:
                raise RuntimeError(
                    "Rulebased balancer can not deal with multiple flex nodes"
                )
        output_df = None
        for time, measurement in df_forecasts.iterrows():
            output = rule_based_logic(measurement, flex_specs)
            if output_df is None:
                output_df = pd.DataFrame(
                    columns=output.index, index=df_forecasts.index
                )
            output_df.loc[time] = output
            flex_specs.initial_sof = output.sof_kwh / flex_specs.agg_cap_flex
        if flex_specs.id is not None:
            output_df.rename(
                {"ptcb_kw": f"ptcb_kw_{flex_specs.id}"}, inplace=True, axis=1
            )
        else:
            output_df.rename({"ptcb_kw": "ptcb_kw_0"}, inplace=True, axis=1)
        return output_df, (SolverStatus.ok, TerminationCondition.optimal)

    if data.uc_name == BM.OPTIMIZER:
        export_limit = None  # If float, this limits the export power
        df_flex_specs = data_input.flex_to_df(flex_specs)
        (
            import_profile,
            pv_profile,
            flex_profiles,
            sof_profiles,
            solver_status
        ) = optimizer_logic(
            df_forecasts.cal_ions_kw,
            df_flex_specs,
            data.bulk,
            export_limit,
        )
        output_df = data_output.prep_optimizer_output(
            import_profile,
            flex_profiles,
            sof_profiles,
            df_forecasts,
            df_flex_specs,
        )
        return output_df, solver_status

def rule_based_logic(measurement: pd.Series, flex_specs: FlexSpecs):
    # initialize
    output_ds = pd.Series(
        index=[
            "ptcb_kw",
            "sof_kwh",
            "import_kw",
            "export_kw",
            "ptei_kw",
            "expected_ptei_kw",
        ],
        dtype=float,
    )
    output_ds.import_kw = 0
    output_ds.export_kw = 0
    output_ds.sof_kwh = 0

    output_ds.ptcb_kw = measurement.preq_kw + measurement.cal_ions_kw
    output_ds.sof_kwh = flex_specs.initial_sof * flex_specs.agg_cap_flex - (
        output_ds.ptcb_kw * measurement.delta_t
    )

    # discharging
    if output_ds.ptcb_kw > 0:
        act_ptcb = output_ds.ptcb_kw
        if abs(output_ds.ptcb_kw) >= flex_specs.pf_dis_kw:
            output_ds.import_kw = output_ds.ptcb_kw - flex_specs.pf_dis_kw
            output_ds.ptcb_kw = flex_specs.pf_dis_kw
            output_ds.sof_kwh = flex_specs.initial_sof * flex_specs.agg_cap_flex - (
                flex_specs.pf_dis_kw * measurement.delta_t
            )

        if output_ds.sof_kwh < flex_specs.min_sof * flex_specs.agg_cap_flex:
            output_ds.import_kw = output_ds.import_kw + (
                (flex_specs.min_sof * flex_specs.agg_cap_flex - output_ds.sof_kwh)
                / measurement.delta_t
            )
            output_ds.ptcb_kw = act_ptcb - output_ds.import_kw
            output_ds.sof_kwh = flex_specs.min_sof * flex_specs.agg_cap_flex

    # charging
    if output_ds.ptcb_kw < 0:
        act_ptcb = output_ds.ptcb_kw
        if abs(output_ds.ptcb_kw) <= flex_specs.pf_ch_kw:
            output_ds.export_kw = 0
            pass
        else:
            output_ds.export_kw = abs(output_ds.ptcb_kw) - flex_specs.pf_ch_kw
            output_ds.ptcb_kw = -flex_specs.pf_ch_kw
            output_ds.sof_kwh = flex_specs.initial_sof * flex_specs.agg_cap_flex + (
                flex_specs.pf_ch_kw * measurement.delta_t
            )
        if output_ds.sof_kwh > flex_specs.max_sof * flex_specs.agg_cap_flex:
            output_ds.export_kw = output_ds.export_kw + (
                (output_ds.sof_kwh - flex_specs.max_sof * flex_specs.agg_cap_flex)
                / measurement.delta_t
            )
            output_ds.ptcb_kw = -(abs(act_ptcb) - (output_ds.export_kw))
            output_ds.sof_kwh = flex_specs.max_sof * flex_specs.agg_cap_flex
        output_ds.ptcb_kw = float(output_ds.ptcb_kw)

    output_ds.ptei_kw = -measurement.cal_ions_kw
    output_ds.expected_ptei_kw = output_ds.export_kw - output_ds.import_kw

    return output_ds

def optimizer_logic(
    ions: pd.Series,
    df_flex: pd.DataFrame,
    bulk_data: Bulk,
    export_limit: float = None,
) -> Tuple[pd.Series, pd.Series, pd.DataFrame, pd.DataFrame, SolverStatus]:
    optimization_solver = SolverFactory("ipopt")  # Selected optimization solver
    start_time = ions.index[0]
    end_time = ions.index[-1]
    delta_T = pd.to_timedelta(ions.index.freq)
    # XXX it is a bit weird to that the end time is not the time of the end
    # but the start of the last interval
    opt_horizon = pd.date_range(
        start_time, end_time + delta_T, freq=delta_T, inclusive="left"
    )
    sof_horizon = pd.date_range(
        start_time, end_time + delta_T, freq=delta_T, inclusive="both"
    )
    bulk_horizon = pd.date_range(
        bulk_data.bulk_start, bulk_data.bulk_end, freq=delta_T, inclusive="both"
    )

    considered_ions_forecast = ions[
        opt_horizon
    ]  # Only the specified time instances in the ion forecast will be taken into account

    #####################################################################################################
    ##################################       OPTIMIZATION MODEL          #################################
    model = ConcreteModel()
    # Index sets
    model.N = list(
        df_flex.index
    )  # Index Set with aggregated flexibility identifiers
    # XXX Not sure why generating a tuple here makes a difference but it does
    model.T = tuple(opt_horizon)
    # Index Set with optimization horizon time step identifiers
    model.T_SoF = tuple(sof_horizon)
    # Index Set with flex horizon time step identifiers
    model.T_bulk = tuple(bulk_horizon)
    # Index Set with bulk horizon time step identifiers
    # Parameters
    model.dT = delta_T  # TimeDelta in one time step
    model.start_time = start_time
    model.end_time = end_time
    model.export_limit = export_limit
    # Forecast parameters
    model.P_Ptei_Forecast = considered_ions_forecast
    # Flex parameters (aggregated flexibility)
    model.FLEX_Min_SoF = df_flex.min_sof
    # Minimum allowable State of Flexibility for flexibility assets
    model.FLEX_Max_SoF = df_flex.max_sof
    # Maximum allowable State of Flexibility for flexibility assets
    model.FLEX_SoF_Value = df_flex.initial_sof
    # SoF value of flex at the beginning of the optimization horizon
    with_fsof = df_flex.with_fsof
    # TODO The constraint that uses this is for all flex, while "with_fsof" is per flex
    if with_fsof.any():
        model.FLEX_Final_SoF = df_flex.final_sof
        model.with_fsof = with_fsof
        # Maximum allowable State of Flexibility for flexibility assets
    model.FLEX_Capacity = df_flex.agg_cap_flex
    # Aggregated Capacity of Flexibility assets (kWsec)
    model.FLEX_Max_Charge_Power = df_flex.pf_ch_kw
    # Max Charge Power of flex (KW)
    model.FLEX_Max_Discharge_Power = df_flex.pf_dis_kw
    # Max Discharge Power of flex (KW)
    model.FLEX_Charging_Eff = df_flex.charge_efficiency
    # Charging efficiency of flex
    model.FLEX_Discharging_Eff = df_flex.discharge_efficiency
    # Discharging efficiency of flex
    # Bulk parameters (bulk energy)
    with_bulk = bulk_data.with_bulk
    if with_bulk:
        model.Bulk_Energy = (
            pd.Series([bulk_data.bulk_energy_kwh]) * 3600
        )  # Bulk energy of Flexibility assets (kWsec)

    # Variables
    model.P_PV_Output = Var(
        model.T, within=NonNegativeReals
    )  # Power output of PV (controlled output)
    model.P_TEI_Output = Var(
        model.T, within=Reals
    )  # Active power exchange with grid (total export/import)
    model.P_FLEX_Output = Var(
        model.N, model.T, within=Reals
    )  # Flex output_active power (discharge positive; charge negative sign)
    model.SoF_FLEX = Var(
        model.N, model.T_SoF, within=NonNegativeReals
    )  # Storage systems' state of charge

    model.con1 = Constraint(model.T, rule=con_rule1)
    model.con3 = Constraint(model.N, model.T, rule=con_rule3)
    model.con4 = Constraint(model.N, rule=con_rule4)
    if with_fsof.any():
        model.con10 = Constraint(model.N, rule=con_rule10)
    if with_bulk:
        model.con11 = Constraint(rule=con_rule11)
    model.con5 = Constraint(model.N, model.T, rule=con_rule5)
    model.con8 = Constraint(model.N, model.T, rule=con_rule8)
    model.con6 = Constraint(model.N, model.T_SoF, rule=con_rule6)
    model.con9 = Constraint(model.N, model.T_SoF, rule=con_rule9)
    model.con7 = Constraint(model.T, rule=con_rule7)

    model.obj = Objective(rule=obj_rule, sense=minimize)

    #####################################################################################################
    ##################################       OPTIMIZATION MODEL          ################################
    solver = optimization_solver.solve(model).solver
    #####################################################################################################
    ##################################       POST PROCESSING             ################################
    flex_supply = pd.DataFrame(index=model.T, columns=df_flex.index)
    sof_supply = pd.DataFrame(index=model.T_SoF, columns=df_flex.index)
    for col in df_flex.index:
        flex_supply[col] = model.P_FLEX_Output[col, :]()
        sof_supply[col] = model.SoF_FLEX[col, :]()
    pv_supply = pd.Series(model.P_PV_Output[:](), index=model.T)
    tei_supply = pd.Series(model.P_TEI_Output[:](), index=model.T)

    return (
        tei_supply,
        pv_supply,
        flex_supply,
        sof_supply,
        (solver.status, solver.termination_condition)
    )
