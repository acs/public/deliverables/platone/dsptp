import json
import pandas as pd
import dateutil.parser
from balancer_utils.balancer_control import Balancer
from balancer_utils.data_output import Write_output

input_file = "/tmp/balancer/input.json"
output_file = "/tmp/balancer/output.json"


def open_json(filename):
    with open(filename) as data_file:
        data = json.load(data_file)
    return data

def write_json_to_file(filename, json_dump):
    with open(filename, "w") as output_file:
        output_file.write(json_dump)


def main():

    data = open_json(input_file)
    output, id = Balancer().balancer_control(data)
    json_output = Write_output().write_json_output(output, id)
    write_json_to_file(output_file, json_output)


if __name__ == "__main__":
    main()




