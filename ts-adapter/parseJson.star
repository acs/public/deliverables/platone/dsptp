load("json.star", "json")
load("logging.star", "log")
load("time.star", "time")

def apply(metric):

    #Get json
    j = json.decode(metric.fields.get("value"))

    #Empty array to be populated with metrics
    metrics = []

    #Info from meter
    name = j["device"]
    timestamp = j["timestamp"]

    #Sweep readings array and append to metrics array
    for readings in j["readings"]:

        #Create new metric object
        new_metric = Metric("mqtt_consumer")

        #Add Meter info to metric
        new_metric.tags["name"] = name

        #Add value and timestamp
        new_metric.fields["data"] = float(readings["data"])
        new_metric.time = time.parse_time(timestamp).unix_nano

        #Add remaining data to metric
        new_metric.tags["component"] = readings["component"]
        new_metric.tags["measurand"] = readings["measurand"]
        new_metric.tags["phase"] = readings["phase"]

        #Append the new metric to the metrics array
        metrics.append(new_metric)

    return metrics