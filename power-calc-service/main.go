package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	_ "embed"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

type reading struct {
	Component string  `json:"component"`
	Measurand string  `json:"measurand"`
	Phase     string  `json:"phase"`
	Data      float64 `json:"data"`
	// Unit      string  `json:unit` // maybe later
}

type PmuReading struct {
	Device    string    `json:"device"`
	Timestamp string    `json:"timestamp"`
	Readings  []reading `json:"readings"`
}

/*
	{
	    "device": "pmu_avacon1",
	    "readings": [
	        {
	            "component": "IL3",
	            "data": 54.804229736328125,
	            "measurand": "magnitude",
	            "phase": "P3"
	        },
	        {
	            "component": "IL3",
	            "data": -0.5466897487640381,
	            "measurand": "angle",
	            "phase": "P3"
	        },
	        {
	            "component": "IL3",
	            "data": 49.97727966308594,
	            "measurand": "frequency",
	            "phase": "P3"
	        },
	        {
	            "component": "UL3",
	            "data": 226.3511962890625,
	            "measurand": "magnitude",
	            "phase": "P3"
	        },
	        {
	            "component": "UL3",
	            "data": -0.7485408782958984,
	            "measurand": "angle",
	            "phase": "P3"
	        },
	        {
	            "component": "UL3",
	            "data": 49.99614715576172,
	            "measurand": "frequency",
	            "phase": "P3"
	        },
	        {
	            "component": "IL2",
	            "data": 84.67153930664063,
	            "measurand": "magnitude",
	            "phase": "P2"
	        },
	        {
	            "component": "IL2",
	            "data": -2.883809804916382,
	            "measurand": "angle",
	            "phase": "P2"
	        },
	        {
	            "component": "IL2",
	            "data": 50.0085334777832,
	            "measurand": "frequency",
	            "phase": "P2"
	        },
	        {
	            "component": "UL2",
	            "data": 226.46560668945313,
	            "measurand": "magnitude",
	            "phase": "P2"
	        },
	        {
	            "component": "UL2",
	            "data": -2.8399910926818848,
	            "measurand": "angle",
	            "phase": "P2"
	        },
	        {
	            "component": "UL2",
	            "data": 49.99687576293945,
	            "measurand": "frequency",
	            "phase": "P2"
	        },
	        {
	            "component": "IL1",
	            "data": 51.397918701171875,
	            "measurand": "magnitude",
	            "phase": "P1"
	        },
	        {
	            "component": "IL1",
	            "data": 1.2730059623718262,
	            "measurand": "angle",
	            "phase": "P1"
	        },
	        {
	            "component": "IL1",
	            "data": 50.02635192871094,
	            "measurand": "frequency",
	            "phase": "P1"
	        },
	        {
	            "component": "UL1",
	            "data": 226.29930114746094,
	            "measurand": "magnitude",
	            "phase": "P1"
	        },
	        {
	            "component": "UL1",
	            "data": 1.3454995155334473,
	            "measurand": "angle",
	            "phase": "P1"
	        },
	        {
	            "component": "UL1",
	            "data": 49.99594497680664,
	            "measurand": "frequency",
	            "phase": "P1"
	        }
	    ],
	    "timestamp": "2022-12-19T14:38:59.739923+00:00"
	}
*/
const unit_mulitplier = 0.001 //Transform from W to KW -> devide by 1000

func ActivePowerSinglePhase(pp *PhasePower, currentMagnitudeMultiplier float64, currentAngleShift float64) int {
	activePower := unit_mulitplier * currentMagnitudeMultiplier * pp.Current.Magnitude * pp.Voltage.Magnitude * math.Cos(pp.Current.Angle-pp.Voltage.Angle+currentAngleShift)
	pp.ActivePower = activePower
	return 0
}

func ReactivePowerSinglePhase(pp *PhasePower, currentMagnitudeMultiplier float64, currentAngleShift float64) int {
	reactivePower := unit_mulitplier * currentMagnitudeMultiplier * pp.Current.Magnitude * pp.Voltage.Magnitude * math.Sin(pp.Current.Angle-pp.Voltage.Angle+currentAngleShift)
	pp.ReactivePower = reactivePower
	return 0
}

func ApparentPowerSinglePhase(pp *PhasePower, currentMagnitudeMultiplier float64) int {
	apparentPower := unit_mulitplier * currentMagnitudeMultiplier * pp.Current.Magnitude * pp.Voltage.Magnitude
	pp.ApparentPower = apparentPower
	return 0
}

type Phasor struct {
	Magnitude float64
	Angle     float64
}

type PhasePower struct {
	Phase         string
	Component     string
	Voltage       Phasor
	Current       Phasor
	ActivePower   float64
	ReactivePower float64
	ApparentPower float64
}

func appendPowerReadings(phase PhasePower, pmuData *PmuReading) {
	var active_power reading
	active_power.Measurand = "magnitude"
	active_power.Data = phase.ActivePower
	active_power.Phase = phase.Phase
	active_power.Component = "P" + phase.Component

	var reactive_power reading
	reactive_power.Measurand = "magnitude"
	reactive_power.Data = phase.ReactivePower
	reactive_power.Phase = phase.Phase
	reactive_power.Component = "Q" + phase.Component

	var apparent_power reading
	apparent_power.Measurand = "magnitude"
	apparent_power.Data = math.Sqrt(phase.ReactivePower*phase.ReactivePower + phase.ActivePower*phase.ActivePower)
	apparent_power.Phase = phase.Phase
	apparent_power.Component = "S" + phase.Component

	(*pmuData).Readings = append((*pmuData).Readings, active_power, reactive_power, apparent_power)
	// bytes, _ := json.MarshalIndent(*pmuData, "", "\t")
	// fmt.Println(string(bytes))
}

// type reading struct {
// 	Component string  `json:"component"`
// 	Measurand string  `json:"measurand"`
// 	Phase     string  `json:"phase"`
// 	Data      float64 `json:"data"`
// }

func appendTotalReadings(phases []PhasePower, pmuData *PmuReading) {
	var total_voltage float64 = 0.0
	var total_current float64 = 0.0
	var total_active_power float64 = 0.0
	var total_reactive_power float64 = 0.0
	var total_apparent_power float64 = 0.0

	for _, phase := range phases {
		total_voltage += phase.Voltage.Magnitude
		total_current += phase.Current.Magnitude
		total_active_power += phase.ActivePower
		total_reactive_power += phase.ReactivePower
		total_apparent_power += phase.ApparentPower
	}

	var total_voltage_reading = reading{"ULT", "magnitude", "total", total_voltage}
	var total_current_reading = reading{"ILT", "magnitude", "total", total_current}
	var total_active_power_reading = reading{"PT", "magnitude", "total", total_active_power}
	var total_reactive_power_reading = reading{"QT", "magnitude", "total", total_reactive_power}
	var total_apparent_power_reading = reading{"ST", "magnitude", "total", total_apparent_power}

	(*pmuData).Readings = append((*pmuData).Readings, total_voltage_reading, total_current_reading, total_active_power_reading, total_reactive_power_reading, total_apparent_power_reading)
	// bytes, _ := json.MarshalIndent(*pmuData, "", "\t")
	// fmt.Println(string(bytes))
}

func extractPhase(data PmuReading, phName string) PhasePower {
	var phaseB PhasePower

	for _, r := range data.Readings {
		if r.Phase == phName {
			// name Component after phase, real syntax has different component names for U and I
			var comp = strings.Replace(phName, "P", "", 1)
			if strings.Contains(r.Component, "I") {
				switch r.Measurand {
				case "magnitude":
					phaseB.Current.Magnitude = r.Data
				case "angle":
					phaseB.Current.Angle = r.Data
				}
			} else if strings.Contains(r.Component, "U") {
				switch r.Measurand {
				case "magnitude":
					phaseB.Voltage.Magnitude = r.Data
				case "angle":
					phaseB.Voltage.Angle = r.Data
				}
			}

			phaseB.Component = comp
			phaseB.Phase = r.Phase
			//fmt.Println(phaseB)
		}
	}

	return phaseB
}

func publishPower(client mqtt.Client, pmuData PmuReading, outTopicSuffix string) {
	data, err := json.MarshalIndent(pmuData, "", " ")
	if err != nil {
		panic(err)
	}
	token := client.Publish(outTopic+outTopicSuffix, 0, false, data)
	token.Wait()

}

func (data *PmuReading) AddPowerAndTotal() {
	var phases = []string{"P1", "P2", "P3"}
	var phasePowerArray [3]PhasePower

	for idx, elem := range phases {
		phasePowerArray[idx] = extractPhase(*data, elem)

		ActivePowerSinglePhase(&phasePowerArray[idx], currentMagnitudeMultiplier, currentAngleShift)
		ReactivePowerSinglePhase(&phasePowerArray[idx], currentMagnitudeMultiplier, currentAngleShift)
		ApparentPowerSinglePhase(&phasePowerArray[idx], currentMagnitudeMultiplier)
		appendPowerReadings(phasePowerArray[idx], data)
	}
	appendTotalReadings(phasePowerArray[:], data)
}

var pmuData mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	//fmt.Printf("#########Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
	outTopicPostfix := strings.TrimPrefix(msg.Topic(), inTopic)
	start := time.Now()
	var data PmuReading
	err := json.Unmarshal(msg.Payload(), &data)
	if err != nil {
		panic(err)
	}
	data.AddPowerAndTotal()
	publishPower(client, data, outTopicPostfix)

	timeElapsed := time.Since(start)
	fmt.Printf("The processing took %s\n", timeElapsed)
}

func mqttConnect() mqtt.Client {
	var broker = getEnv("MQTT_BROKER", "localhost")
	var port = getEnv("MQTT_PORT", "1883")
	var podIP = getEnv("KUBERNETES_POD_IP", "127.0.0.1")

	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("mqtt://%s:%s", broker, port))
	opts.SetClientID("powercalc_" + strings.ReplaceAll(podIP, ".", "_"))
	opts.SetUsername(getEnv("MQTT_USER", "admin"))
	opts.SetPassword(getEnv("MQTT_PSSWD", "admin"))
	opts.SetDefaultPublishHandler(messagePubHandler)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	opts.SetAutoReconnect(true)
	opts.SetMaxReconnectInterval(10 * time.Second)
	opts.SetReconnectingHandler(func(c mqtt.Client, options *mqtt.ClientOptions) {
		fmt.Println("...... mqtt reconnecting ......")
	})
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	return client
}

// extended getEnv that also takes a default value if env is not set
func getEnv(key, defaultValue string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = defaultValue
	}
	return value
}

var inTopic = getEnv("MQTT_INPUT_TOPIC", "/dev/pmu")
var outTopic = getEnv("MQTT_OUTPUT_TOPIC", "/dev/power")
var currentMagnitudeMultiplier, errCMM = strconv.ParseFloat(getEnv("CURRENT_MAGNITUDE_MULTIPLIER", "1.059"), 64)
var currentAngleShift, errCAS = strconv.ParseFloat(getEnv("CURRENT_ANGLE_SHIFT", "-0.078"), 64)

//go:generate bash get_version.sh
//go:embed version.txt
var version string

func main() {

	// very simple info flag for kubernetes probes
	if len(os.Args) > 1 && os.Args[1] == "info" {
		fmt.Println("powercalc version", version)
		return
	}

	mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", 0)
	// mqtt.CRITICAL = log.New(os.Stdout, "[CRIT] ", 0)
	// mqtt.WARN = log.New(os.Stdout, "[WARN]  ", 0)
	// mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)

	// inTopic := getEnv("MQTT_INPUT_TOPIC", "/dev/pmu")
	// outTopic = getEnv("MQTT_OUTPUT_TOPIC", "/dev/power")
	if errCMM != nil {
		panic(errCMM)
	}
	if errCAS != nil {
		panic(errCAS)
	}
	client := mqttConnect()
	client.AddRoute(inTopic+"/#", pmuData)

	token := client.Subscribe(inTopic+"/#", 1, nil)
	token.Wait()
	fmt.Printf("Subscribed to input topic %s\n", inTopic)
	fmt.Printf("Publishing to output topic %s\n", outTopic)

	// wait for graceful termination
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	<-sigChan

	client.Disconnect(250)
	fmt.Println("Sucessfully terminated...")
}
