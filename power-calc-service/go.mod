module sogno/power-calc

go 1.18

require github.com/eclipse/paho.mqtt.golang v1.3.5

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/exp v0.0.0-20230206171751-46f607a40771 // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
)
