package main

import (
	"encoding/json"
	"fmt"
	"testing"

	"golang.org/x/exp/slices"
)

func getPmuData() PmuReading {
	var jsonData = "{\"device\":\"pmu_avacon1\",\"readings\":[{\"component\":\"IL3\",\"data\":54.804229736328125,\"measurand\":\"magnitude\",\"phase\":\"P3\"},{\"component\":\"IL3\",\"data\":-0.5466897487640381,\"measurand\":\"angle\",\"phase\":\"P3\"},{\"component\":\"IL3\",\"data\":49.97727966308594,\"measurand\":\"frequency\",\"phase\":\"P3\"},{\"component\":\"UL3\",\"data\":226.3511962890625,\"measurand\":\"magnitude\",\"phase\":\"P3\"},{\"component\":\"UL3\",\"data\":-0.7485408782958984,\"measurand\":\"angle\",\"phase\":\"P3\"},{\"component\":\"UL3\",\"data\":49.99614715576172,\"measurand\":\"frequency\",\"phase\":\"P3\"},{\"component\":\"IL2\",\"data\":84.67153930664063,\"measurand\":\"magnitude\",\"phase\":\"P2\"},{\"component\":\"IL2\",\"data\":-2.883809804916382,\"measurand\":\"angle\",\"phase\":\"P2\"},{\"component\":\"IL2\",\"data\":50.0085334777832,\"measurand\":\"frequency\",\"phase\":\"P2\"},{\"component\":\"UL2\",\"data\":226.46560668945313,\"measurand\":\"magnitude\",\"phase\":\"P2\"},{\"component\":\"UL2\",\"data\":-2.8399910926818848,\"measurand\":\"angle\",\"phase\":\"P2\"},{\"component\":\"UL2\",\"data\":49.99687576293945,\"measurand\":\"frequency\",\"phase\":\"P2\"},{\"component\":\"IL1\",\"data\":51.397918701171875,\"measurand\":\"magnitude\",\"phase\":\"P1\"},{\"component\":\"IL1\",\"data\":1.2730059623718262,\"measurand\":\"angle\",\"phase\":\"P1\"},{\"component\":\"IL1\",\"data\":50.02635192871094,\"measurand\":\"frequency\",\"phase\":\"P1\"},{\"component\":\"UL1\",\"data\":226.29930114746094,\"measurand\":\"magnitude\",\"phase\":\"P1\"},{\"component\":\"UL1\",\"data\":1.3454995155334473,\"measurand\":\"angle\",\"phase\":\"P1\"},{\"component\":\"UL1\",\"data\":49.99594497680664,\"measurand\":\"frequency\",\"phase\":\"P1\"}],\"timestamp\":\"2022-12-19T14:38:59.739923+00:00\"}"
	var data PmuReading
	err := json.Unmarshal([]byte(jsonData), &data)
	if err != nil {
		panic(err)
	}
	return data
}

func TestExtractPhase(t *testing.T) {
	var data = getPmuData()
	var phasePower = extractPhase(data, "P1")
	if phasePower.Current.Magnitude != 51.397918701171875 {
		t.Errorf("incorrect current magnitude epected 51.397918701171875 but read %v", phasePower.Current.Magnitude)
	}
	if phasePower.Current.Angle != 1.2730059623718262 {
		t.Errorf("incorrect current angle epected 1.2730059623718262 but read %v", phasePower.Current.Angle)
	}
	if phasePower.Voltage.Magnitude != 226.29930114746094 {
		t.Errorf("incorrect voltage magnitude epected 226.29930114746094 but read %v", phasePower.Voltage.Magnitude)
	}
	if phasePower.Voltage.Angle != 1.3454995155334473 {
		t.Errorf("incorrect current angle epected 1.3454995155334473 but read %v", phasePower.Voltage.Angle)
	}
}

func TestAppendPowerReadings(t *testing.T) {
	var data = getPmuData()
	var phase = "P1"
	var phasePower PhasePower

	phasePower = extractPhase(data, phase)

	ActivePowerSinglePhase(&phasePower, currentMagnitudeMultiplier, currentAngleShift)
	ReactivePowerSinglePhase(&phasePower, currentMagnitudeMultiplier, currentAngleShift)
	ApparentPowerSinglePhase(&phasePower, currentMagnitudeMultiplier)
	appendPowerReadings(phasePower, &data)
	fmt.Printf("%+v\n", data.Readings)
	var expected_apparent_power = reading{Component: "S1", Measurand: "magnitude", Phase: "P1", Data: 12.317560554377252}
	var expected_reactive_power = reading{Component: "Q1", Measurand: "magnitude", Phase: "P1", Data: -1.84672412761097}
	var expected_active_power = reading{Component: "P1", Measurand: "magnitude", Phase: "P1", Data: 12.178337653688615}
	if !slices.Contains(data.Readings, expected_apparent_power) {
		t.Error("final Readings did not contain apparent power or apparent power was invalid.")
	}
	if !slices.Contains(data.Readings, expected_reactive_power) {
		t.Error("final Readings did not contain reactive power or reactive power was invalid.")
	}
	if !slices.Contains(data.Readings, expected_active_power) {
		t.Error("final Readings did not contain active power or active power was invalid.")
	}
}

func TestAppendTotals(t *testing.T) {
	var data = getPmuData()
	data.AddPowerAndTotal()
	fmt.Printf("%+v\n", data.Readings)
	var expected_voltage = reading{Component: "ULT", Measurand: "magnitude", Phase: "total", Data: 679.1161041259766}
	var expected_current = reading{Component: "ILT", Measurand: "magnitude", Phase: "total", Data: 190.87368774414062}
	var expected_apparent_power = reading{Component: "ST", Measurand: "magnitude", Phase: "total", Data: 45.76098650967866}
	var expected_reactive_power = reading{Component: "QT", Measurand: "magnitude", Phase: "total", Data: -2.6914621371904106}
	var expected_active_power = reading{Component: "PT", Measurand: "magnitude", Phase: "total", Data: 45.370652065630495}

	if !slices.Contains(data.Readings, expected_voltage) {
		t.Error("final Readings did not contain total voltage or total voltage was invalid.")
	}
	if !slices.Contains(data.Readings, expected_current) {
		t.Error("final Readings did not contain total current or total current was invalid.")
	}
	if !slices.Contains(data.Readings, expected_apparent_power) {
		t.Error("final Readings did not contain total apparent power or total apparent power was invalid.")
	}
	if !slices.Contains(data.Readings, expected_reactive_power) {
		t.Error("final Readings did not contain total reactive power or total reactive power was invalid.")
	}
	if !slices.Contains(data.Readings, expected_active_power) {
		t.Error("final Readings did not contain total reactive power or total active power was invalid.")
	}
}
