# install latest version via helm
## Prerequisits
1. Kubernetes or k3s available and correct namspace set as default.
2. [Helm installed](https://helm.sh/docs/intro/install/)

## Steps
1. Add sogno repo 
```bash
> helm repo add sogno https://sogno-platform.github.io/helm-charts/
```
2. Adjust `values.yaml`. E.g. add ingress.
3. Create secrets by filling the files `secret_<name>.yaml`
4. a. run `make deploy` 
    
    b. alternatively apply secrets and install via helm
    ```bash
    > kubectl -n platone-wp5-balancer-dev-v4 apply -f secret_redis.yaml
    > kubectl -n platone-wp5-balancer-dev-v4 apply -f secret_api.yaml
    > helm install -n platone-wp5-balancer-dev-v4 -f values.yaml pymfm sogno/pymfm
    ```