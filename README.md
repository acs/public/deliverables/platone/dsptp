# DSO Technical Platform Deployment [![](https://img.shields.io/badge/version-0.3.0-blue)](https://git.rwth-aachen.de/acs/public/deliverables/platone/dsptp)

This repository contains deployment instructions and corresponding configuration files for the Platone DSO Technical Platform.
The core platform is based on the SOGNO platform.
We assume you have a full-fleged or light-weight kubernetes cluster up and running. 
Please ensure you setup is in line with [this](https://sogno-platform.github.io/docs/getting-started/single-node/) base setup.

## Visualization Stack

### Databus

```bash
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm repo update
$ helm install -n dsotp --create-namespace -f databus/rabbitmq_values.yaml rabbitmq bitnami/rabbitmq
```

### Timeseries Database

```bash
$ helm repo add influxdata https://influxdata.github.io/helm-charts
$ helm repo update
$ helm install influxdb influxdata/influxdb -n dsotp -f ts-database/influxdb-helm-values.yaml
```

Find pod

```bash
$ kubectl --namespace dsotp get pods
```

Login to the pod

```bash
# kubectl --namespace dsotp exec -i -t [pod name] /bin/sh
```


Run influxdb CLI

```bash
$ influx
```

Create database and user telegraf and grant access

```sql
> CREATE DATABASE telegraf
> SHOW Databases
> CREATE USER telegraf WITH PASSWORD 'telegraf'
> GRANT ALL ON "telegraf" TO "telegraf"
```

### TS-DB Adapter
Create configmaps and deploy telegraf using the configmap

```bash
$ kubectl apply -k ts-adapter/
$ kubectl apply -f ts-adapter/telegraf-deployment.yaml
```

### Grafana

```bash
$ helm install grafana stable/grafana -f visualization/grafana_values.yaml
```

Get admin password

```bash
$ kubectl get secret -n dsotp grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

## License

This project is licensed to the [Apache Software Foundation (ASF)](http://www.apache.org/licenses/LICENSE-2.0).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the Apache License version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Apache License for more details.

You should have received a copy of the Apache License
along with this program.  If not, see <http://www.apache.org/licenses/LICENSE-2.>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Funding
<img alt="PLATONE" style="border-width:0" src="docs/platone_logo.png" height="63"/></a>&nbsp; 
<a rel="funding" href="https://cordis.europa.eu/project/id/864300"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Platone" href="https://platone-h2020.eu/">PLATform for Operation of distribution NEtworks </a> (Platone), funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/864300"> grant agreement No. 864300</a>.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
